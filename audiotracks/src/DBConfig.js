export const DBConfig = {
    name: "AudioDB",
    version: 1,
    objectStoresMeta: [
      {
        store: "audiotracks",
        storeConfig: { keyPath: "id", autoIncrement: true },
        storeSchema: [
          { name: "data", keypath: "data", options: { unique: false } },
          { name: "filename", keypath: "filename", options: { unique: false } }
        ]
      }
    ]
  };
  