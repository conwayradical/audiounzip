import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useIndexedDB } from "react-indexed-db";
import JSZip, { files } from "jszip";
import JSZipUtils from "./JSZipUtils";
import soundfile from "./rocksteady.mp3";

// might have needed FilReader, but didn't in the end
function getBase64(files) {
  return new Promise(function (resolve, reject) {
    var reader = new FileReader();
    reader.onload = function () {
      resolve(reader.result.substr(reader.result.indexOf(",") + 1));
    };
    reader.onerror = reject;
    //console.log("files", files);
    //reader.readAsDataURL(files);
    reader.readAsArrayBuffer(files);
  });
}

export function AudioUnzip() {
  const [audiostate, setAudioState] = useState([
    { data: "", filename: "", id: "" },
  ]);
  const [playbackValueInput, setPlaybackValue] = useState("1");
  const [loopstartInput, setLoopstartValue] = useState("");
  const [loopendInput, setLoopendValue] = useState("");
  const { add, update, getByID, getAll, clear } = useIndexedDB("audiotracks");
  //let history = useNavigate();

  let audioCtx = [];
  let audioCtxx = [];
  let source = [];
  let playSoundBuffer;
  let songLength;

  const pre = document.querySelector("pre");
  const myScript = document.querySelector("script");
  const play = document.querySelector(".play");
  const stop = document.querySelector(".stop");

  const playbackControl = document.querySelector(".playback-rate-control");
  const playbackValue = document.querySelector(".playback-rate-value");
  //playbackControl.setAttribute("disabled", "disabled");

  const loopstartControl = document.querySelector(".loopstart-control");
  const loopstartValue = document.querySelector(".loopstart-value");
  //loopstartControl.setAttribute("disabled", "disabled");

  const loopendControl = document.querySelector(".loopend-control");
  const loopendValue = document.querySelector(".loopend-value");
  //loopendControl.setAttribute("disabled", "disabled");


  useEffect(() => {
    // Get data once page loaded.
    if (audiostate[0] && audiostate[0].data === "") {
      getAll().then((audioFromDB) => {
        console.log("audioFromDB=", audioFromDB);
        if (audioFromDB) {
          setAudioState(audioFromDB);
        }
      });
    }
  });

  const loadAudio = () => {
    // console.log("load Audio", audiostate);
    // delete local indexDB and audiostate
    setAudioState([]);
    clear().then(() => {
      console.log("All Clear!");
    });
    getAudioTracks();
  };

  const getAudioTracks = () => {
    JSZipUtils.getBinaryContent(
      "https://3firetrackmanager.s3.amazonaws.com/com.3firemusic.choirplayer.000174/1205.zip",
      function (err, data) {
        if (err) {
          throw err;
        }

        const jsZip = new JSZip();
        jsZip.loadAsync(data).then(function (zip) {
          //console.log("zip.files", zip.files);
          const promiseArray = Object.keys(zip.files).map(function (filename) {
            return new Promise(function (resolve, reject) {
              zip.files[filename]
                .async("arraybuffer")
                .then(async function (fileData) {

                  // might have needed to convert with Filereader
                  //let promise = getBase64(fileData);
                  //let encoded_file4 = await promise;
                  //return { data: encoded_file4, filename: filename }

                  resolve({ data: fileData, filename: filename });
                });
            });
          });
          Promise.all(promiseArray).then(function (audiotracks) {
            console.log(audiotracks);
            //setAudioState(
            const audioArray = audiotracks.map((audiodata) => {
              return new Promise(function (resolve, reject) {
                add(audiodata).then(
                  (key) => {
                    console.log("id generated: ", key);
                    let newtrack = { ...audiodata, id: key };
                    //webAudio(newtrack)
                    resolve(newtrack);
                  },
                  (error) => {
                    console.log(error);
                  }
                );
              });
            });
            //);
            Promise.all(audioArray).then(function (audiotracks) {
              setAudioState(audiotracks);
            });
          });
        });
      }
    );
  };

  const handleChange = (evt) => {
    console.log("new value", evt.target.value);

    if (evt.target.name === "playbackValue") {
      setPlaybackValue(evt.target.value);
      const rate = evt.target.value;
      source.forEach((track) => {
        track.playbackRate.value = rate;
      });

      /*
      const source = new AudioBufferSourceNode();
      const rate = evt.target.value;
      source.playbackRate.value = rate;
      console.log(source.playbackRate.value === rate);
      */
    }
    if (evt.target.name === "loopstart") setLoopstartValue(evt.target.value);
    if (evt.target.name === "loopend") setLoopendValue(evt.target.value);
  };

  const playAudio = (evt) => {
    createPlayers();
    //testmp3();
    play.setAttribute("disabled", "disabled");
    playbackControl.removeAttribute("disabled");
    loopstartControl.removeAttribute("disabled");
    loopendControl.removeAttribute("disabled");
  };

  const stopAudio = () => {
    source.forEach((track) => {
      track.stop(0);
    });
    source = [];
    play.removeAttribute("disabled");
    //playbackControl.setAttribute("disabled", "disabled");
    //loopstartControl.setAttribute("disabled", "disabled");
    //loopendControl.setAttribute("disabled", "disabled");
  };

  const testmp3 = () => {
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    audioCtx = new AudioContext();

    var request = new XMLHttpRequest();
    request.open("GET", soundfile, true);
    console.log("request", request);
    request.responseType = "arraybuffer";
    request.onload = function () {
      console.log("request.response", request.response);
      audioCtx.decodeAudioData(
        request.response,
        function (buffer) {
          playSoundBuffer = buffer;
          playTestSound(); // don't start processing it before the response is there!
        },
        function (error) {
          console.error("decodeAudioData error", error);
        }
      );
    };
    request.send(); //start doing something async
  };

  const playTestSound = () => {
    var source = audioCtx.createBufferSource();
    source.buffer = playSoundBuffer; // This is the line that generates the error
    source.connect(audioCtx.destination);
    source.start(0);
  };

  const createPlayers = () => {
    //create an array of audioCtx according to length of audio data array
    let i = 0;
    audiostate.forEach(() => {
      audioCtxx = new AudioContext();
      let blob = new Blob([audiostate[i].data]);
      let url = URL.createObjectURL(blob);
      //console.log('url',url)
      var request = new XMLHttpRequest();
      request.open("GET", url, true);
      //console.log('request', request)
      request.responseType = "arraybuffer";
      request.onload = () => {
        //console.log('request.response',request.response)
        audioCtxx.decodeAudioData(
          request.response,
          (buffer) => {
            playSoundBuffer = buffer;
            playSound(); // don't start processing it before the response is there!
          },
          function (error) {
            console.error("decodeAudioData error", error);
          }
        );
      };
      request.send(); //start doing something async
      i++;
    });

    const playSound = () => {
      source.push(audioCtxx.createBufferSource());
      source[source.length - 1].buffer = playSoundBuffer; // This is the line that generates the error
      //console.log('playbackControl.value', playbackControl.value)
      source[source.length - 1].playbackRate.value = playbackControl.value;
      source[source.length - 1].loop = true;
      source[source.length - 1].connect(audioCtxx.destination);
      source[source.length - 1].start(0);
      //loopstartControl.setAttribute('max', Math.floor(songLength));
      //loopendControl.setAttribute('max', Math.floor(songLength));
    };
  };

  return (
    <div>
      <div>
        <button onClick={loadAudio}>LoadAudio</button>
      </div>
      <div>
        <button className="play" onClick={playAudio}>
          Play
        </button>
        <button className="stop" onClick={stopAudio}>
          Stop
        </button>
        <h2>Set playback rate</h2>
        <input
          className="playback-rate-control"
          type="range"
          min="0.25"
          max="3"
          step="0.05"
          value={playbackValueInput}
          name="playbackValue"
          onChange={handleChange}
        />
        <span className="playback-rate-value">{playbackValueInput}</span>

        <h2>Set loop start and loop end</h2>
        <input
          className="loopstart-control"
          type="range"
          min="0"
          max="20"
          step="1"
          value={loopstartInput}
          name="loopstart"
          onChange={handleChange}
        />
        <span className="loopstart-value">{loopstartInput}</span>

        <input
          className="loopend-control"
          type="range"
          min="0"
          max="20"
          step="1"
          value={loopendInput}
          name="loopend"
          onChange={handleChange}
        />
        <span className="loopend-value">{loopendInput}</span>

        <pre></pre>
      </div>
    </div>
  );
}
