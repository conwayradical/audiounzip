import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { initDB } from "react-indexed-db";
import { DBConfig } from "./DBConfig";
import { AudioUnzip } from "./AudioUnzip";
import './App.css';

initDB(DBConfig);

function App() {
  return (
    <Router>
      <div className="App">
        <h1>Audio Loader</h1>
        <hr />
        <Routes>
          <Route exact path="/" element={<AudioUnzip/>} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
